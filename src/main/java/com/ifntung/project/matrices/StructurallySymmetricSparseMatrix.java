package com.ifntung.project.matrices;

import java.util.ArrayList;

public class StructurallySymmetricSparseMatrix extends SparseMatrix{


    ArrayList<Integer> AD, AV, AL;

    public StructurallySymmetricSparseMatrix(int[][] inputMatrix, NumerationConvention numerationConvention) throws IllegalArgumentException {
        super(inputMatrix, numerationConvention);
    }

    /** Structurally Symmetric matrix storage
     *  <br><a href="https://docs.oracle.com/cd/E19205-01/819-5268/plug_sparse.html"> Oracle Documentation </a><br>
     * LI and LJ is coordinates of AV (above  diagonal elements)
     * **/
    public void transformToSS() throws IllegalArgumentException
    {
        AD= new ArrayList<>(); AV= new ArrayList<>(); AL = new ArrayList<>(); LI = new ArrayList<>(); LJ = new ArrayList<>();


        if(!isMatrixSS()) throw new IllegalArgumentException("Matrix isnt Structurally Symmetric. Cant transform");

        for (int i = 0; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix[i].length; j++) {
                if(inputMatrix[i][j] != 0){
                    if (j > i){           AV.add(inputMatrix[i][j]); LI.add(i+INC); LJ.add(j+INC);   }
                    else if (j == i)      AD.add(inputMatrix[i][j]);
                    else                  AL.add(inputMatrix[i][j]);
                }
            }
        }
    }

    private boolean isMatrixSS()
    {
        for (int i = 0; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix[i].length; j++) {
                if(inputMatrix[i][j] == 0 && inputMatrix[j][i] != 0) return false;
                if(inputMatrix[i][j] != 0 && inputMatrix[j][i] == 0) return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "SparseMatrices{" +
                "\n AD=" + AD +
                "\n AV=" + AV +
                "\n AL=" + AL +
                ",\n LI=" + LI +
                ",\n LJ=" + LJ +
                '}';
    }
}
