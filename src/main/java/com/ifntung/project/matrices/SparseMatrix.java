package com.ifntung.project.matrices;


import java.util.ArrayList;

public class SparseMatrix {
    int[][] inputMatrix;

    ArrayList<Integer> A; // inpNotNullElements
    ArrayList<Integer> LI; // inpRowIndexes
    ArrayList<Integer> LJ; // inpColumnIndexes
    NumerationConvention numerationConvention;
    final int INC; //INACCURACY неточність, похибка


    /** @param numerationConvention is important to receive result with specified value increasing**/
    public SparseMatrix(int[][] inputMatrix, NumerationConvention numerationConvention) throws IllegalArgumentException {
        if(inputMatrix == null) throw new IllegalArgumentException("Wrong input matrix");
        this.inputMatrix = inputMatrix.clone();
        this.numerationConvention = numerationConvention;
        INC = numerationConvention.ordinal();
    }

    /**Coordinate list **/
    public void transformToCOO()
    {
        A = new ArrayList<>(); LI = new ArrayList<>(); LJ = new ArrayList<>();

        for (int i = 0; i < inputMatrix.length; i++)
        {
            for (int j = 0; j < inputMatrix[i].length; j++)
            {
                if (inputMatrix[i][j] != 0)
                {
                    A.add(inputMatrix[i][j]);
                    LI.add(i + INC); // if numerationConvention PROGRAMMING = 0, else = 1;
                    LJ.add(j + INC);
                }
            }
        }
    }

    /**Compessed Row Storage
     *  <br> <a href="http://homepage.cs.uiowa.edu/~sriram/21/fall07/code/CRS.java"> example without using Collection </a>
     * **/
    public void transformToCRS()
    {
        //initialization
        A = new ArrayList<>(); LI = new ArrayList<>(); LJ = new ArrayList<>();

        // point to the next position in val to store the value
        int index = 0+INC;

        for (int i = 0; i < inputMatrix.length; i++)
        {
            //if row is null we duplicate count of elements in array
            LI.add(index);
            for (int j = 0; j < inputMatrix[i].length; j++)
            {
                if (inputMatrix[i][j] != 0)
                {
                    A.add(inputMatrix[i][j]);
                    LJ.add(j + INC);
                    index++;

                }
            }
        }
        //4. останій елемент LI – кількість не нульових елементів + 1.
        LI.add(index);
    }

    /**Compessed Column Storage or Compressed sparse column
     * <br> <a href="https://docs.oracle.com/cd/E19205-01/819-5268/plug_sparse.html">Sun Performance Library User’s Guide</a> **/
    public void transformToCCS()
    {
        A = new ArrayList<>(); LI = new ArrayList<>(); LJ = new ArrayList<>();

        // point to the next position in val to store the value
        int index = 0+INC;
        // to avoid duplication after crossing sec. row
        int totalCollumnCounter = 0;
        for (int i = 0; i < inputMatrix.length; i++)
        {

            for (int j = 0; j < inputMatrix[i].length; j++) {
                //3. місце першого не нульового елемент в кожному стовпці записують в оновимірний масив LJ;
                if(totalCollumnCounter < inputMatrix[i].length){ LJ.add(index); totalCollumnCounter++; }
                    if (inputMatrix[i][j] != 0)
                    {
                        //1. всі ненульові елементи aij порядково записуються в одновимірний масив А;
                        A.add(inputMatrix[i][j]);
                        //2. перший індекс кожного ненульового елемента записується в одновимірний масив LI;
                        LI.add(i + INC);
                        index++;
                    }
                }
        }
        //4. останій елемент LJ – кількість не нульових елементів.
        LJ.add(--index);
    }


    /** Working only for {@link  #transformToCRS() transformToCRS} !!!
     * **/
    public int getElement(int iIndex, int jIndex) throws IllegalArgumentException{

        int i = iIndex - INC;
        int j = jIndex - INC;

        if(i < 0 || j < 0 || A == null || LI == null || LJ == null)
            throw new IllegalArgumentException("Matrix wasn`t transformed");

        int matrixElement = 0; // значення штучного елемента
        final int N1 = LI.get(i) - INC;
        final int N2 = LI.get(i+1) - INC;

        //System.out.println("N1 = "+ N1 + " N2 = "+ N2);
        for (int k = N1; k < N2; k++)
        {
            //System.out.println("k = "+k);
            if (LJ.get(k)-INC == j)
            {
                return matrixElement = A.get(k);
            }
            else if(LJ.get(k)-INC > j)
                return 0;
        }
        return matrixElement;

    }



    @Override
    public String toString() {
        return "SparseMatrices{" +
                "\n A=" + A +
                ",\n LI=" + LI +
                ",\n LJ=" + LJ +
                '}';
    }
}
