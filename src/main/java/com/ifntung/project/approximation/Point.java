package com.ifntung.project.approximation;

public class Point {
    private double x;
    private double y;
    public Point(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public boolean similar(Point point)
    {
        if(point.x == this.x && point.y == this.y)
            return true;
            else return false;
    }
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
