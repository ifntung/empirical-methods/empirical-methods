package com.ifntung.project.approximation;

//апроксимація лінії за допомогою лінійної функції
//y = kx+b
public class Line {
    public double k, b;
    public Line(double k, double b) {
    this.k = k;
    this.b = b;
    }
    public boolean isPointBelongToLine(Point point)
    {
        return point.getY() == (k*point.getX()) + b;
    }

    //інтерполяція точок
    public Point[] generatePointsOnLine()
    {
        Point[] points = new Point[10];
        for(int i = 0; i < 10; i++)
        {
            double x = i;
            double y = (k*x) + b;
            points[i] = new Point(x, y);
        }
        return points;
    }

    //чи пересікаются дві лінії
    public Point intersection(Line line2) {
        if(this.k == line2.k) return null;

        //https://javascopes.com/java-intersection-of-two-lines-76213c48/
        //https://overcoder.net/q/2057717/java-%D0%BD%D0%B0%D0%B9%D1%82%D0%B8-%D0%BF%D0%B5%D1%80%D0%B5%D1%81%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B4%D0%B2%D1%83%D1%85-%D0%BB%D0%B8%D0%BD%D0%B8%D0%B9

        double x = (line2.b - this.b) / (this.k - line2.k);
        double y = this.k * x + this.b;
        Point res = new Point((int) x, (int) y);
        System.out.println(res.toString());
        return res;
       // throw new UnsupportedOperationException();
    }

}
