package com.ifntung.project.approximation;

import java.util.ArrayList;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

// Знаходження рівняння лінійної регресії
public class LinearRegression {
    ArrayList<Point> points = new ArrayList<>();
    double xMed; double yMed;
    double xPowMed; double yPowMed;

    double xMultiplyByYMed;

    double sPowX;  double sPowY;
    double sX; double sY;

    public double rColeration;
    int elementsCount;

    public LinearRegression(ArrayList<Point> points) {
        this.points = points;
        elementsCount = points.size()-1;
        double x; double y;
        for(int i = 0; i < elementsCount; i++)
        {
            x = points.get(i).getX();
            y = points.get(i).getY();
            xMed+=x;
            yMed+=y;

            xPowMed+=pow(x, 2);
            yPowMed+=pow(y, 2);

            xMultiplyByYMed += x*y;
        }
        xMed /= elementsCount;
        yMed /= elementsCount;

        xPowMed /= elementsCount;
        yPowMed /= elementsCount;

        xMultiplyByYMed /=elementsCount;

        //System.out.println("xMed= " + xMed + "; yMed= " + yMed + "; xPowMed= " + xPowMed + "; yPowMed= " + yPowMed + "; xMultiplyByY= " + xMultiplyByY);

        //---------------------\\
        sPowX = xPowMed - pow(xMed, 2);
        sPowY = yPowMed - pow(yMed, 2);

        sX = sqrt(sPowX);
        sY = sqrt(sPowY);

        //System.out.println("sPowX= " + sPowX + "; sPowY= " + sPowY + "; sX= " + sX + "; sY= " + sY);

        //---------------------\\

        rColeration = ( xMultiplyByYMed - (xMed *yMed) )/( sX * sY );

        //System.out.println("rColeration= " + rColeration );

        findRegressinYOnX();
        findRegressinXOnY();
    }
    //Знайти вибіркові рівняння прямих ліній регресії Y на X
    //і X на Y за даними таблиці спостережень

    //Метод максимуму правдоподібності (метод найкращого наближення)

        double a; double b; double c; double d;
    // знайти рівняння регресії Y на X (y=a*x+b)
        public Line findRegressinYOnX(){
            a = rColeration*(sY/sX);
            b = yMed-(a*xMed);
            return new Line(a, b);
        }

    // знайти рівняння регресії X на Y (x=c*y+d)
        public Line findRegressinXOnY(){
            c = rColeration*(sX/sY);
            d = xMed - (c*yMed);

            //для подання на ту ж лінію координат, рівняння слід трансформувати
            // y=x1 / с – d / с
            double a2; double b2;
            a2 = points.get(0).getX() / c;
            b2 = d/c;
            return new Line(a2, b2);
        }

    // Знаходження методом найменших квадратів
    // https://en.wikipedia.org/wiki/Least_squares
         public Line leastSquaresMethod() {
             //формула виду - y=a0+a1*x
             double a0; double a1;
             a1 = ( xMultiplyByYMed-(xMed*yMed) )/( xPowMed-pow(xMed, 2) );
             a0 = yMed-(a1*xMed);

             //необхідно змінні поміняти місцями
            return new Line(a1, a0);
         }
}
