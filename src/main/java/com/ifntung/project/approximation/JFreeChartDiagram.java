package com.ifntung.project.approximation;

import javax.swing.*;

import org.jfree.chart.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import org.jfree.chart.block.BlockBorder;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

// працює тільки з мейну!!!
//https://www.youtube.com/watch?v=7ukZUJKNCC4&ab_channel=MarceloJosu%C3%A9Telles
//https://stackoverflow.com/questions/27667889/drawing-a-diagram-with-jfreechart/27775799
// добавити відображення координат мишки при наведенні - https://stackoverflow.com/questions/1512112/jfreechart-get-mouse-coordinates
public class JFreeChartDiagram extends JFrame {

        private static final long serialVersionUID = 1L;

        public JFreeChartDiagram(String applicationTitle, String chartTitle, XYDataset dataset) {
           super(applicationTitle);

            // based on the dataset we create the chart
            JFreeChart pieChart = ChartFactory.createXYLineChart(chartTitle, "X", "Y",
                    dataset,PlotOrientation.VERTICAL, true, true, false);

            // Adding chart into a chart panel
            //ChartPanel chartPanel = new ChartPanel(pieChart);

            // settind default size
            //chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));

            // add to contentPane
            //setContentPane(chartPanel);

            XYPlot plot = pieChart.getXYPlot();

            var renderer = new XYLineAndShapeRenderer();

            renderer.setSeriesPaint(0, Color.RED);
            renderer.setSeriesStroke(0, new BasicStroke(2.0f));
            renderer.setSeriesPaint(1, Color.BLUE);
            renderer.setSeriesStroke(1, new BasicStroke(2.0f));
            // зробити всі лінії невидимими
            //renderer.setDefaultLinesVisible(false);
            renderer.setSeriesLinesVisible(1, false);

            renderer.setSeriesPaint(2, Color.DARK_GRAY);
            renderer.setSeriesStroke(2, new BasicStroke(2.0f));
            renderer.setSeriesPaint(3, Color.DARK_GRAY);
            renderer.setSeriesStroke(3, new BasicStroke(2.0f));

            plot.setRenderer(renderer);
            plot.setBackgroundPaint(Color.white);
            plot.setRangeGridlinesVisible(false);
            plot.setDomainGridlinesVisible(false);

            pieChart.getLegend().setFrame(BlockBorder.NONE);

            ChartFrame frame1 = new ChartFrame("Графік координат", pieChart);
            //frame1.addMouseListener(mouseListener);

            frame1.setVisible(true);
            frame1.setSize(1300, 800);
        }

        //коли клікаєм на елемент, вивести його координати
        MouseListener mouseListener = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                report(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                report(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
            private void report(MouseEvent chartMouseEvent) {

                System.err.println("X:" + chartMouseEvent.getLocationOnScreen().getX() + ", Y:" + chartMouseEvent.getLocationOnScreen().getY());
              /*  ChartEntity ce = chartMouseEvent.getPoint();
                if (ce instanceof XYItemEntity) {
                    XYItemEntity e = (XYItemEntity) ce;
                    XYDataset d = e.getDataset();
                    int s = e.getSeriesIndex();
                    int i = e.getItem();
                    System.out.println("X:" + d.getX(s, i) + ", Y:" + d.getY(s, i));
                }*/
            }
        };

       /* ChartMouseListener mouseListener = new ChartMouseListener() {
        @Override
        public void chartMouseClicked(ChartMouseEvent e) {
            report(e);
        }
        public void chartMouseMoved(ChartMouseEvent cme) {
            //report(cme);
        }
        private void report(ChartMouseEvent chartMouseEvent) {
            ChartEntity ce = chartMouseEvent.getEntity();
            if (ce instanceof XYItemEntity) {
                XYItemEntity e = (XYItemEntity) ce;
                XYDataset d = e.getDataset();
                int s = e.getSeriesIndex();
                int i = e.getItem();
                System.out.println("X:" + d.getX(s, i) + ", Y:" + d.getY(s, i));
            }
        }
    };*/
        private XYDataset createDataset() {

            final XYSeries P0 = new XYSeries("P0");
            P0.add(0, 1);
            P0.add(10, 5);

            P0.add(35, 10);
            P0.add(50, 25);
            P0.add(85, 50);
            P0.add(110, 80);


            final XYSeries H = new XYSeries("H");
            H.add(10, 1);
            H.add(20, 15);
            H.add(45, 40);
            H.add(100, 80);


            final XYSeries wait = new XYSeries("wait");
            wait.add(80, 23);
            wait.add(90, 46);
            wait.add(105, 125);



            final XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(P0);
            dataset.addSeries(H);
            dataset.addSeries(wait);

            return dataset;

        }
}
