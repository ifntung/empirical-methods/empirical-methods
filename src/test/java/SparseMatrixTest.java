
import com.ifntung.project.matrices.NumerationConvention;
import com.ifntung.project.matrices.SparseMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SparseMatrixTest {
    int sparseMatrix[][]
            = {
            {0, 0, 3, 0, 4},
            {0, 0, 5, 7, 0},
            {0, 0, 0, 0, 0},
            {0, 2, 6, 0, 0}
    };

    int sparseMatrix2[][]
            = {
            {0, 2, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 5, 0, 0, 0, 1},
            {0, 0, 0, 0, 9, 1},
            {0, 0, 0, 0, 0, 0},
            {0, 8, 0, 4, 0, 3}
    };

    int sparseMatrix3[][]
            = {
            {1, 3, 0, 0},
            {2, 4, 0, 7},
            {0, 0, 6, 0},
            {0, 5, 0, 8}
    };
    int sparseMatrix4[][]
            = {
            {0, 2, 0, 1, 0, 0, 0, 2, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            {0, 5, 0, 0, 0, 1, 5, 2, 0, 0,},
            {0, 0, 0, 3, 9, 1, 0, 0, 0, 9,},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            {0, 8, 0, 6, 0, 3, 0, 0, 0, 0,},
            {0, 1, 0, 0, 0, 1, 5, 0, 4, 0,},
            {0, 3, 0, 2, 0, 1, 5, 0, 0, 0,},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            {0, 1, 0, 0, 0, 2, 5, 0, 9, 0,},
    };



    @Test
    public void testMatrixTransformToCOO()  {
        try {
            SparseMatrix sparseMatrix = new SparseMatrix(sparseMatrix2, NumerationConvention.MATHEMATICS);
           sparseMatrix.transformToCOO();
           System.out.println("COO Matrix");
           System.out.println(sparseMatrix);

        } catch(IllegalArgumentException e){
            System.err.println("Err data insertion. " + e.getMessage());
        }
    }

    @Test
    public void testMatrixTransformToCRS()  {
        try {
            SparseMatrix sparseMatrix = new SparseMatrix(sparseMatrix2, NumerationConvention.PROGRAMING);

            sparseMatrix.transformToCRS();
            System.out.println("CRS Matrix, Last element LI is count of elements + 1");
            System.out.println(sparseMatrix);

            //System.out.println("Get elem = " + sparseMatrix.getElement(3, 2));
            //System.out.println("Get elem = " + sparseMatrix.getElement(2, 3));
            //System.out.println("Get elem = " + sparseMatrix.getElement(4, 4));
            //System.out.println("Get elem = " + sparseMatrix.getElement(6, 2));
            //PROGRAMING NumerationConvention
            System.out.println("Get elem = " + sparseMatrix.getElement(2, 1));
            System.out.println("Get elem = " + sparseMatrix.getElement(1, 2));
            System.out.println("Get elem = " + sparseMatrix.getElement(3, 3));
            System.out.println("Get elem = " + sparseMatrix.getElement(5, 1));

        } catch(IllegalArgumentException e){
            System.err.println("Err data insertion. " + e.getMessage());
        }
    }

    @Test
    public void testMatrixTransformToCCS()  {
        try {
            SparseMatrix sparseMatrix = new SparseMatrix(sparseMatrix2, NumerationConvention.MATHEMATICS);

            sparseMatrix.transformToCCS();
            System.out.println("CCS Matrix, Last element LJ is count of elements");
            System.out.println(sparseMatrix);

            //System.out.println("Get elem = " + sparseMatrix.getElement(3, 2));

        } catch(IllegalArgumentException e){
            System.err.println("Err data insertion. " + e.getMessage());
        }
    }



}
