import com.ifntung.project.approximation.JFreeChartDiagram;
import com.ifntung.project.approximation.Line;
import com.ifntung.project.approximation.LinearRegression;
import com.ifntung.project.approximation.Point;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RegressionTest {

     static ArrayList<Point> points = new ArrayList<>();
    static ArrayList<Point> pointsWithMush = new ArrayList<>();

    static ArrayList<Point> regressionLine = new ArrayList<>();
    static ArrayList<Point> regressionLine2 = new ArrayList<>();

    //генерація шуму поряд із справжніми значеннями

    /**
     *
     * @param count кількість точок для генерації
     * @param method якщо true, то використовуэтся рівномірний розподіл, у випадку false - за Гаусом
     *
     */
    public static void generateMush(int count, boolean method) {

        for(int i = 0; i < count; i++) {

            //Вибірка випадкового числа від 0 до 10, яке означає індекс існуючої точки
            final Random random = new Random();
            int indexOfRandomRealPoint = random.nextInt(points.size()-1);

            double starterX = points.get(indexOfRandomRealPoint).getX();
            double starterY = points.get(indexOfRandomRealPoint).getY();

            // Нормальний розподіл
            // або розподіл за Гаусом
            if(!method) {
                double x = starterX + NormalDistribution(-1, 1);
                ; //+ random.nextDouble(0,1);//
                double y = starterY + NormalDistribution(-1, 1);
                ; //+ random.nextDouble(0,1);//NormalDistribution(0, 1);
                pointsWithMush.add(new Point(x, y));
                System.out.print("Point(" + x +", " +y + ") ");
            }
            // Рівномірний розподіл
            // при рівномірному розподілі розкид значень получаєтся меншим
            if(method) {
                double x = starterX + random.nextDouble(-1, 1);
                double y = starterY + random.nextDouble(-1, 1);
                pointsWithMush.add(new Point(x, y));
                System.out.print("Point(" + x +", " +y + ") ");
            }



        }



    }

    //JFreeChart 1.5.3 API
    @Test
    public static void generatePointsTest() {
        Line line = new Line(1, 2);
        points.addAll(  Arrays.stream(line.generatePointsOnLine()).toList() );
        //System.out.println(pointsToString(points));
        generateMush(200, true);
        //System.out.println(pointsToString(pointsWithMush));
    }
    @Test
    public static void linearRegressionTest() {
        LinearRegression linearRegression = new LinearRegression(pointsWithMush);

        regressionLine.addAll(Arrays.stream(
                linearRegression.findRegressinXOnY().generatePointsOnLine()
        ).toList());
        regressionLine2.addAll(Arrays.stream(
                linearRegression.findRegressinYOnX().generatePointsOnLine()
        ).toList());
        //System.out.println(pointsToString(regressionLine));

    }

    //Регресійний аналіз за методом найменших квадратів
    @Test
    public static void linearRegByLeastSquaresMethodTest() {

        LinearRegression linearRegression = new LinearRegression(pointsWithMush);

        regressionLine.addAll(Arrays.stream(
                linearRegression.leastSquaresMethod().generatePointsOnLine()
        ).toList());

        //System.out.println(pointsToString(regressionLine));

    }
    public static void main(String[] args) {
        generatePointsTest();
        linearRegressionTest();
        //linearRegByLeastSquaresMethodTest();
        JFreeChartDiagram chart = new JFreeChartDiagram("Вибіркове рівняння прямої лінії регресії",
                "Вибіркове рівняння прямої лінії регресії", packRegressionPointsData());
    }


    public static XYSeriesCollection packPointsData(){

        final XYSeries realPoints = new XYSeries("realPoints");
        for (Point e : points){
            realPoints.add(e.getX(), e.getY());
        }
        final XYSeries mushPoints = new XYSeries("mushPoints");
        for (Point e : pointsWithMush){
            mushPoints.add(e.getX(), e.getY());
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(realPoints);
        dataset.addSeries(mushPoints);

        return dataset;
    }

    public static XYSeriesCollection packRegressionPointsData(){

        final XYSeries realPoints = new XYSeries("realPoints");
        for (Point e : points){
            realPoints.add(e.getX(), e.getY());
        }
        final XYSeries mushPoints = new XYSeries("mushPoints");
        for (Point e : pointsWithMush){
            mushPoints.add(e.getX(), e.getY());
        }

        final XYSeries regressionPoints = new XYSeries("regressionPoints");
        for (Point e : regressionLine){
            regressionPoints.add(e.getX(), e.getY());
        }
        final XYSeries regressionPoints2 = new XYSeries("regressionPoints2");
        for (Point e : regressionLine2){
            regressionPoints2.add(e.getX(), e.getY());
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(realPoints);
        dataset.addSeries(mushPoints);

        dataset.addSeries(regressionPoints);
        dataset.addSeries(regressionPoints2);

        return dataset;
    }
    public static String pointsToString(List<Point> points) {
        StringBuilder st = new StringBuilder();
        for (Point e : points) {
            st.append(e.toString());
            st.append("\n");
        }
        return st.toString();
    }

    /**
     * Звичайний нормальний розподіл
     * @param u мінімальне значення
     * @param v дисперсія
     * @return згенероване випадкове значення
     */
    public static double NormalDistribution(float u, float v) {

        double scale = Math.pow(10, 3);

        // Алгоритм Бокса – Мюллера / Гауса
        // Перетворення набору значень рівномірного розподілу
        // згенерованого засобами бібліотеки Math
        // в нормально розподілений

        double x1 = Math.random();
        double x2 = Math.random();

        //вбудовані засоби генерації в бібліотеці Math
        //double rand= random.nextGaussian();
        double rand=Math.sqrt(-2*Math.log(x1))*Math.cos(2*Math.PI*x2);
        double res = v * rand + u;

        return Math.ceil(res * scale) / scale; //округлення до 3х знаків

    }
}
