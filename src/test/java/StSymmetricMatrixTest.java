import com.ifntung.project.matrices.NumerationConvention;
import com.ifntung.project.matrices.StructurallySymmetricSparseMatrix;
import org.junit.jupiter.api.Test;

public class StSymmetricMatrixTest {

    int sparseMatrix2[][]
            = {
            {0, 2, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 5, 0, 0, 0, 1},
            {0, 0, 0, 0, 9, 1},
            {0, 0, 0, 0, 0, 0},
            {0, 8, 0, 4, 0, 3}
    };
    int StructurallySymmetricSparseMatrix[][]
            = {
            {1, 3, 0, 0},
            {2, 4, 0, 7},
            {0, 0, 6, 0},
            {0, 5, 0, 8}
    };
    int StructurallySymmetricSparseMatrix2[][]
            = {
            {1, 3, 0, 0, 0},
            {2, 4, 0, 7, 0},
            {0, 0, 6, 5, 0},
            {0, 5, 2, 8, 1},
            {0, 0, 0, 8, 1}
    };

    @Test
    public void testTransformNonStSymmetricMatrixToStSymmetric(){
        //Input matrix isn`t Symetrix
        try {
            StructurallySymmetricSparseMatrix sparseMatrix =
                    new StructurallySymmetricSparseMatrix(sparseMatrix2, NumerationConvention.MATHEMATICS);
            sparseMatrix.transformToSS();

        } catch(IllegalArgumentException e){
            System.err.println("Err data insertion. " + e.getMessage());
        }
    }
    @Test
    public void testStructurallySymmetricSparseMatrix()  {

        //Transform Symmetric Sparse Matrix
        try {
            StructurallySymmetricSparseMatrix sparseMatrix2 =
                    new StructurallySymmetricSparseMatrix(StructurallySymmetricSparseMatrix2, NumerationConvention.MATHEMATICS);

            sparseMatrix2.transformToSS();

            System.out.println("Structurally Symmetric Sparse Matrix");
            System.out.println(sparseMatrix2);

            //System.out.println("Get elem = " + sparseMatrix.getElement(3, 2));
        } catch(IllegalArgumentException e){
            System.err.println("Err data insertion. " + e.getMessage());
        }
    }
}
